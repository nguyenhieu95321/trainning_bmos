import React from 'react';
import {useHistory} from "react-router-dom";
// import logo from "../../assets/images/farmer.png"
// import logo1 from "../../assets/images/logo2.png"
// import logo2 from "../../assets/images/logoblack1.png"
// import logo3 from "../../assets/images/Picture3.jpg"
import logo4 from "../../assets/images/logoblack.png"
import Device from "../Device";
import "../Device/style.css"
import {AppContext} from "../../Context/AppContext";
import Counter from "../Counter";
import ErrorBoundary from "../../ErrorBoundary";

export default function Index() {
    // const [theme, setTheme] = React.useState('dark')
    const {theme, setTheme} = React.useContext(AppContext)
    const history = useHistory();
    const changeTheme = () => {
      setTheme(theme === 'dark' ? 'light' : 'dark');
    }
    return (
        <div>
            {/*<img src={logo}/>*/}
            {/*<img src={logo1}/>*/}
            {/*<img src={logo2}/>*/}
            {/*<img src={logo3}/>*/}
            <img src={logo4} />

            <button onClick={() => history.push('/')}>Đăng Xuất</button>
            <button className={theme} onClick={() => changeTheme()}>Chế độ tối</button>
            <ErrorBoundary>
                <Counter/>
            </ErrorBoundary>
            {/*<Device theme={theme}/>*/}
            <Device/>
        </div>
    );
}

