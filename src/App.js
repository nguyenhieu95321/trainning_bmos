import './App.css';
import React, {Suspense} from "react";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import {ADMIN, LOGIN} from "./components/value_const";
import Admin from "./components/Admin"
import Login from "./components/Login"
import AppProvider from "./Context/AppContext";


// const Admin = React.lazy(() => import('./components/Admin'))
// const Login = React.lazy(() => import('./components/Login'))

function App() {
    return (
        <BrowserRouter>
            <AppProvider>
            {/*<Suspense fallback={<div>Loading....</div>}>*/}
                <Switch>
                    <Route component={Admin} path={ADMIN}/>
                    <Route component={Login} path={LOGIN}/>
                </Switch>
            {/*</Suspense>*/}
            </AppProvider>
        </BrowserRouter>
    );
}

export default App;
