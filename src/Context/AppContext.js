import React from 'react';

// tạo context
export const AppContext = React.createContext();
export default function AppProvider({children}) {
    const [theme, setTheme] = React.useState('dark')
    return (
        // tạo Provider
        <AppContext.Provider value={{
            setTheme, theme
        }}>
            {/*consumer*/}
            {children}
        </AppContext.Provider>
    );
}